# NetifyOS Installer

## Description

The anaconda installer is used by NetifyOS.  Visit Red Hat's project page for details - https://fedoraproject.org/wiki/Anaconda

## Changes for NetifyOS

TODO

## Maintenance

To pull in an upstream update, follow these instructions:

* git clone git@bitbucket.org/eglooca/anaconda.git
* cd anaconda
* git checkout c7
* git remote add upstream git://git.centos.org/rpms/anaconda.git
* git pull upstream c7
* git checkout netifyos7
* git merge --no-commit c7
* git commit
